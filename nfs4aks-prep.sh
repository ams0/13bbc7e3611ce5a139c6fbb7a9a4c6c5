#!/bin/bash

RG=nfs
REGION=uksouth
ADDRESS_PREFIX_STORAGE="172.16.0.0/16"
SUBNET_PREFIX_STORAGE="172.16.10.0/24"
ADDRESS_PREFIX_AKS="172.17.0.0/16"
SUBNET_PREFIX_AKS="172.17.10.0/24"

az group create -n $RG -l $REGION

#network
az network vnet create -l $REGION -g $RG -n nfsnet  --address-prefixes $ADDRESS_PREFIX_STORAGE
az network vnet subnet create -g $REGION  --vnet-name nfsnet -n storage --address-prefixes $SUBNET_PREFIX_STORAGE

az network vnet create -l $REGION -g $RG -n aksnet  --address-prefixes $ADDRESS_PREFIX_AKS
az network vnet subnet create -g nfs  --vnet-name aksnet -n aks --address-prefixes $SUBNET_PREFIX_AKS

az network vnet peering create -g $RG -n nfs2aks --remote-vnet aksnet --vnet-name nfsnet --allow-forwarded-traffic --allow-vnet-access
az network vnet peering create -g $RG -n aks2nfs --remote-vnet nfsnet --vnet-name aksnet --allow-forwarded-traffic --allow-vnet-access

#storage
az storage account create -l $REGION -g $RG --sku Premium_LRS --kind FileStorage -n nfs4aks --https-only false
